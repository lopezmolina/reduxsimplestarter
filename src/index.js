import React from 'react'
import ReactDom from 'react-dom'
import YTSearch from 'youtube-api-search'
import SearchBar from './components/search_bar'
import VideoList from './components/video_list'
const API_KEY = 'AIzaSyBSJIIti_okF319eWLoxfGBREg0CrWwwPk';

// Creación del componente principal (que contiene los otros componentes y se renderiza en el contenedor principal del DIM)
class App extends React.Component{
	constructor(props) {
		super(props);

		this.state = {videos : [] };

		YTSearch({key: API_KEY, term: 'gatitos'}, (videos) => {
			this.setState({ videos });
			// this.setState({videos: videos}); //because ES6
			// This is a ES6 trick when the key and the property have the same name
		});
	}

	render(){
		return(
			<div> 
				<h1>Hola mundo</h1>
				<SearchBar />
				<VideoList videos = {this.state.videos} />
			</div>
			);
	}
}

ReactDom.render(<App/>, document.querySelector('.container'));
