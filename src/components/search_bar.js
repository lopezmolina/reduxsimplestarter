import React from 'react'

 export default class SearchBar extends React.Component{
	constructor(props) {
		super(props);

		this.state ={term:'',number:1};
	}
	render(){
		return (
			<div>
				<input 
				value = {this.state.term}
				onChange={event => {
					this.setState({term : (event.target.value)});
				}}
				/> 
				<br/>

				You typed: {this.state.term}
			</div>
		);
	}
};

// export default SearchBar;